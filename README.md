# php-extended/php-api-fr-insee-ban-object

A library that implements the php-extended/php-api-fr-insee-ban-interface library.

This library makes a collaborative work of the ApiFrInsee, Dgfip, OpenStreetMap
Contributors, La Poste and Etalab ; known as the BAN (Base des Adresses Nationale)
available as objects to populate a database. The BAN is available at the url :
[https://adresse.data.gouv.fr/](https://adresse.data.gouv.fr/).

![coverage](https://gitlab.com/php-extended/php-api-fr-insee-ban-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-insee-ban-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-insee-ban-object ^8`


## Basic Usage

This library gives an unique endpoint : `\PhpExtended\Insee\InseeCogEndpoint`
from which all data can be retrieved.

- For most usages, you may use the following code :

```php

use PhpExtended\Insee\InseeBanEndpoint;
use PhpExtended\Endpoint\GzipHttpEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/** $client \Psr\Http\Client\ClientInterface */

$endpoint = new ApiFrInseeBanEndpoint(new GzipHttpEndpoint(new HttpEndpoint($client)));

$lastDateTime = $endpoint->getLatestDate();
$departements = $endpoint->getDepartementIds($lastDateTime);

foreach($departements as $departement)
{
	$banLineIterator = $endpoint->getBanLinesIterator($lastDateTime, $departement);
	
	foreach($banLineIterator as $banLine)
	{
		/** @var $banLine \PhpExtended\Insee\InseeBanLine */
		// do something with the ban line representing :
		// - a commune with CodeInsee and NomCommune
		// - a group (street) with Id, NomVoie and NomComplementaire
		// - an address with Id, Numero, Suffixe, CodePostal
		// - a position with Id, X, Y, Lat, Lon
		// - and generic data like type of localisation,
		//     source and date of last update
	}
}

```

Be wary that the client must follow a certain number of rules regarding the
handling of files via the `X-Php-Download-File` request and response
headers for the `GzipHttpEndpoint` to be able to unzip the downloaded
file without having to use gigabytes of memory. It must also return an 
`X-Request-Uri` header to get the full uri back.


For an example of the minimal needed http client, look at the 
`InseeBanEndpointTest` class file and find the client that is used.


If you have that much memory however, you can bypass the `GzipHttpEndpoint`
and all of the `X-Php-Download-File` and `X-Request-Uri` shenanigans
altogether.


## License

- The code is under MIT (See [license file](LICENSE)).

- The data is under Open License ([English](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf) / [Français](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf))
