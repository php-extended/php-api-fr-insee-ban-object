<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLocalisation;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLocalisationParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeBanLocalisationParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeBan\ApiFrInseeBanLocalisationParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeBanLocalisationParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeBanLocalisationParser
	 */
	protected ApiFrInseeBanLocalisationParser $_object;
	
	public function testParseSuccess() : void
	{
		$expected = new ApiFrInseeBanLocalisation(5, 'parcel', 5, 'Parcelle : parcelle cadastrale');
		
		$this->assertEquals($expected, $this->_object->parse('parcel'));
	}
	
	public function testParseFails() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('r&b');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeBanLocalisationParser();
	}
	
}
