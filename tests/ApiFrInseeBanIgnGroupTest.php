<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan\Test;

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnGroup;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeBanIgnGroupTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnGroup
 * @internal
 * @small
 */
class ApiFrInseeBanIgnGroupTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeBanIgnGroup
	 */
	protected ApiFrInseeBanIgnGroup $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdBanGroup() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdBanGroup());
		$this->_object->setIdBanGroup('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdBanGroup());
	}
	
	public function testGetIdIgnGroup() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdIgnGroup());
		$this->_object->setIdIgnGroup('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdIgnGroup());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeBanIgnGroup('azertyuiop', 'azertyuiop');
	}
	
}
