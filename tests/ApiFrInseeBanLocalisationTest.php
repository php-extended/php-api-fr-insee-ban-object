<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan\Test;

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLocalisation;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeBanLocalisationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeBan\ApiFrInseeBanLocalisation
 * @internal
 * @small
 */
class ApiFrInseeBanLocalisationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeBanLocalisation
	 */
	protected ApiFrInseeBanLocalisation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(12, $this->_object->getId());
		$this->_object->setId(25);
		$this->assertEquals(25, $this->_object->getId());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCode());
		$this->_object->setCode('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getCode());
	}
	
	public function testGetPrecision() : void
	{
		$this->assertEquals(12, $this->_object->getPrecision());
		$this->_object->setPrecision(25);
		$this->assertEquals(25, $this->_object->getPrecision());
	}
	
	public function testGetDefinition() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getDefinition());
		$this->_object->setDefinition('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getDefinition());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeBanLocalisation(12, 'azertyuiop', 12, 'azertyuiop');
	}
	
}
