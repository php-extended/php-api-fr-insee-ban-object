<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanSource;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanSourceParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeBanSourceParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeBan\ApiFrInseeBanSourceParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeBanSourceParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeBanSourceParser
	 */
	protected ApiFrInseeBanSourceParser $_object;
	
	public function testParseSuccess() : void
	{
		$expected = new ApiFrInseeBanSource(1, 'dgfip', 'Direction Générale des Finances Publiques');
		
		$this->assertEquals($expected, $this->_object->parse('dgfip'));
	}
	
	public function testParseFails() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('r&b');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeBanSourceParser();
	}
	
}
