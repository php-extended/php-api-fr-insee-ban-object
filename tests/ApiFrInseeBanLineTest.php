<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLine;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanLocalisation;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanSource;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeBanLineTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeBan\ApiFrInseeBanLine
 * @internal
 * @small
 */
class ApiFrInseeBanLineTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeBanLine
	 */
	protected ApiFrInseeBanLine $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdBanPosition() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdBanPosition());
		$this->_object->setIdBanPosition('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdBanPosition());
	}
	
	public function testGetIdBanAdresse() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdBanAdresse());
		$this->_object->setIdBanAdresse('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdBanAdresse());
	}
	
	public function testGetCleInterop() : void
	{
		$this->assertNull($this->_object->getCleInterop());
		$this->_object->setCleInterop('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getCleInterop());
	}
	
	public function testGetIdBanGroup() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdBanGroup());
		$this->_object->setIdBanGroup('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdBanGroup());
	}
	
	public function testGetIdFantoir() : void
	{
		$this->assertNull($this->_object->getIdFantoir());
		$this->_object->setIdFantoir('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdFantoir());
	}
	
	public function testGetNumero() : void
	{
		$this->assertEquals(12, $this->_object->getNumero());
		$this->_object->setNumero(25);
		$this->assertEquals(25, $this->_object->getNumero());
	}
	
	public function testGetSuffixe() : void
	{
		$this->assertNull($this->_object->getSuffixe());
		$this->_object->setSuffixe('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getSuffixe());
	}
	
	public function testGetNomVoie() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNomVoie());
		$this->_object->setNomVoie('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getNomVoie());
	}
	
	public function testGetCodePostal() : void
	{
		$this->assertNull($this->_object->getCodePostal());
		$this->_object->setCodePostal('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getCodePostal());
	}
	
	public function testGetNomCommune() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNomCommune());
		$this->_object->setNomCommune('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getNomCommune());
	}
	
	public function testGetCodeInsee() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCodeInsee());
		$this->_object->setCodeInsee('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getCodeInsee());
	}
	
	public function testGetNomComplementaire() : void
	{
		$this->assertNull($this->_object->getNomComplementaire());
		$this->_object->setNomComplementaire('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getNomComplementaire());
	}
	
	public function testGetPosName() : void
	{
		$this->assertNull($this->_object->getPosName());
		$this->_object->setPosName('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getPosName());
	}
	
	public function testGetX() : void
	{
		$this->assertEquals(14.1, $this->_object->getX());
		$this->_object->setX(15.2);
		$this->assertEquals(15.2, $this->_object->getX());
	}
	
	public function testGetY() : void
	{
		$this->assertEquals(14.1, $this->_object->getY());
		$this->_object->setY(15.2);
		$this->assertEquals(15.2, $this->_object->getY());
	}
	
	public function testGetLon() : void
	{
		$this->assertEquals(14.1, $this->_object->getLon());
		$this->_object->setLon(15.2);
		$this->assertEquals(15.2, $this->_object->getLon());
	}
	
	public function testGetLat() : void
	{
		$this->assertEquals(14.1, $this->_object->getLat());
		$this->_object->setLat(15.2);
		$this->assertEquals(15.2, $this->_object->getLat());
	}
	
	public function testGetTypLoc() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrInseeBanLocalisation::class)->disableOriginalConstructor()->getMock(), $this->_object->getTypLoc());
		$this->_object->setTypLoc($this->getMockBuilder(ApiFrInseeBanLocalisation::class)->disableOriginalConstructor()->getMock());
		$this->assertEquals($this->getMockBuilder(ApiFrInseeBanLocalisation::class)->disableOriginalConstructor()->getMock(), $this->_object->getTypLoc());
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrInseeBanSource::class)->disableOriginalConstructor()->getMock(), $this->_object->getSource());
		$this->_object->setSource($this->getMockBuilder(ApiFrInseeBanSource::class)->disableOriginalConstructor()->getMock());
		$this->assertEquals($this->getMockBuilder(ApiFrInseeBanSource::class)->disableOriginalConstructor()->getMock(), $this->_object->getSource());
	}
	
	public function testGetDateDerMajGroup() : void
	{
		$this->assertEquals('2000-01-01 00:00:01', $this->_object->getDateDerMajGroup()->format('Y-m-d H:i:s'));
		$date = new DateTimeImmutable();
		$this->_object->setDateDerMajGroup($date);
		$this->assertEquals($date, $this->_object->getDateDerMajGroup());
	}
	
	public function testGetDateDerMajHn() : void
	{
		$this->assertEquals('2000-01-01 00:00:01', $this->_object->getDateDerMajHn()->format('Y-m-d H:i:s'));
		$date = new DateTimeImmutable();
		$this->_object->setDateDerMajHn($date);
		$this->assertEquals($date, $this->_object->getDateDerMajHn());
	}
	
	public function testGetDateDerMajPos() : void
	{
		$this->assertEquals('2000-01-01 00:00:01', $this->_object->getDateDerMajPos()->format('Y-m-d H:i:s'));
		$date = new DateTimeImmutable();
		$this->_object->setDateDerMajPos($date);
		$this->assertEquals($date, $this->_object->getDateDerMajPos());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeBanLine('azertyuiop', 'azertyuiop', 'azertyuiop', 12, 'azertyuiop', 'azertyuiop', 'azertyuiop', 14.1, 14.1, 14.1, 14.1, $this->getMockBuilder(ApiFrInseeBanLocalisation::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrInseeBanSource::class)->disableOriginalConstructor()->getMock(), DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'));
	}
	
}
