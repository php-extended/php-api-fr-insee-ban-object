<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan\Test;

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnAddress;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeBanIgnAddressTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeBan\ApiFrInseeBanIgnAddress
 * @internal
 * @small
 */
class ApiFrInseeBanIgnAddressTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeBanIgnAddress
	 */
	protected ApiFrInseeBanIgnAddress $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdBanAdresse() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdBanAdresse());
		$this->_object->setIdBanAdresse('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdBanAdresse());
	}
	
	public function testGetIdIgnAdresse() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdIgnAdresse());
		$this->_object->setIdIgnAdresse('qsdfghjklm');
		$this->assertEquals('qsdfghjklm', $this->_object->getIdIgnAdresse());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeBanIgnAddress('azertyuiop', 'azertyuiop');
	}
	
}
