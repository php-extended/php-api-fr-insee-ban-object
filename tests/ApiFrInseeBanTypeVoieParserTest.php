<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoie;
use PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeBanTypeVoieParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeBan\ApiFrInseeBanTypeVoieParser
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeBanTypeVoieParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeBanTypeVoieParser
	 */
	protected ApiFrInseeBanTypeVoieParser $_object;
	
	public function testResultFound() : void
	{
		$expected = new ApiFrInseeBanTypeVoie(6, 'CHE', 'Chemin');
		
		$this->assertEquals($expected, $this->_object->parse('CHE'));
	}
	
	public function testExpectedFound() : void
	{
		$expected = new ApiFrInseeBanTypeVoie(6, 'CHE', 'Chemin');
		
		$this->assertEquals($expected, $this->_object->parse('CHEM1'));
	}
	
	public function testResultNotFound() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('XYZ');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeBanTypeVoieParser();
	}
	
}
