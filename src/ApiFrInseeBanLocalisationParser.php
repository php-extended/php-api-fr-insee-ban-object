<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * ApiFrInseeBanLocalisationParser class file.
 * 
 * This class parses insee ban localisation data into localisation objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeBanLocalisation>
 */
class ApiFrInseeBanLocalisationParser extends AbstractParser
{
	
	/**
	 * The localisations.
	 * 
	 * @var array<string, ApiFrInseeBanLocalisation>
	 */
	protected array $_localisations = [];
	
	/**
	 * Builds a new ApiFrInseeBanLocalisationParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/localisation.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeBanLocalisation::class, $iterator);
		
		/** @var ApiFrInseeBanLocalisation $localisation */
		foreach($iterator as $localisation)
		{
			$this->_localisations[(string) $localisation->getCode()] = $localisation;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeBanLocalisation
	{
		$data = (string) $data;
		
		if(isset($this->_localisations[$data]))
		{
			return $this->_localisations[$data];
		}
		
		throw new ParseException(ApiFrInseeBanLocalisation::class, $data, 0);
	}
	
}
