<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * ApiFrInseeBanSourceParser class file.
 * 
 * This class parses insee ban source data into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeBanSource>
 */
class ApiFrInseeBanSourceParser extends AbstractParser
{
	
	/**
	 * The sources.
	 * 
	 * @var array<string, ApiFrInseeBanSource>
	 */
	protected array $_sources = [];
	
	/**
	 * Builds a new ApiFrInseeBanSourceParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/source.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeBanSource::class, $iterator);
		
		/** @var ApiFrInseeBanSource $source */
		foreach($iterator as $source)
		{
			$this->_sources[(string) $source->getCode()] = $source;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeBanSource
	{
		$data = (string) $data;
		
		if(isset($this->_sources[$data]))
		{
			return $this->_sources[$data];
		}
		
		throw new ParseException(ApiFrInseeBanSource::class, $data, 0);
	}
	
}
