<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

/**
 * ApiFrInseeBanLocalisation class file.
 * 
 * This is a simple implementation of the ApiFrInseeBanLocalisationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeBanLocalisation implements ApiFrInseeBanLocalisationInterface
{
	
	/**
	 * The id of the localisation.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The code of the type localisation.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The precision of the type localisation (lower is better).
	 * 
	 * @var int
	 */
	protected int $_precision;
	
	/**
	 * The definition of the type localisation (fr).
	 * 
	 * @var string
	 */
	protected string $_definition;
	
	/**
	 * Constructor for ApiFrInseeBanLocalisation with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param int $precision
	 * @param string $definition
	 */
	public function __construct(int $id, string $code, int $precision, string $definition)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setPrecision($precision);
		$this->setDefinition($definition);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the localisation.
	 * 
	 * @param int $id
	 * @return ApiFrInseeBanLocalisationInterface
	 */
	public function setId(int $id) : ApiFrInseeBanLocalisationInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the localisation.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of the type localisation.
	 * 
	 * @param string $code
	 * @return ApiFrInseeBanLocalisationInterface
	 */
	public function setCode(string $code) : ApiFrInseeBanLocalisationInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the type localisation.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the precision of the type localisation (lower is better).
	 * 
	 * @param int $precision
	 * @return ApiFrInseeBanLocalisationInterface
	 */
	public function setPrecision(int $precision) : ApiFrInseeBanLocalisationInterface
	{
		$this->_precision = $precision;
		
		return $this;
	}
	
	/**
	 * Gets the precision of the type localisation (lower is better).
	 * 
	 * @return int
	 */
	public function getPrecision() : int
	{
		return $this->_precision;
	}
	
	/**
	 * Sets the definition of the type localisation (fr).
	 * 
	 * @param string $definition
	 * @return ApiFrInseeBanLocalisationInterface
	 */
	public function setDefinition(string $definition) : ApiFrInseeBanLocalisationInterface
	{
		$this->_definition = $definition;
		
		return $this;
	}
	
	/**
	 * Gets the definition of the type localisation (fr).
	 * 
	 * @return string
	 */
	public function getDefinition() : string
	{
		return $this->_definition;
	}
	
}
