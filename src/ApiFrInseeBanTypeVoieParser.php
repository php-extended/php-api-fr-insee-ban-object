<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;

/**
 * ApiFrInseeBanTypeVoieParser class file.
 * 
 * This class parses insee sirene type voie data into source objects.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiFrInseeBanTypeVoie>
 */
class ApiFrInseeBanTypeVoieParser extends AbstractParser
{
	
	/**
	 * The types voies.
	 * 
	 * @var array<string, ApiFrInseeBanTypeVoie>
	 */
	protected array $_typesVoies = [];
	
	/**
	 * Builds a new ApiFrInseeBanTypeVoieParser with the given data.
	 * 
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct()
	{
		$reifier = new Reifier();
		$filePath = \dirname(__DIR__).'/data/type_voie.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		$iterator = $reifier->reifyIterator(ApiFrInseeBanTypeVoie::class, $iterator);
		
		/** @var ApiFrInseeBanTypeVoie $typeVoie */
		foreach($iterator as $typeVoie)
		{
			$this->_typesVoies[(string) $typeVoie->getCode()] = $typeVoie;
			
			// {{{ hacks for duplicated code
			foreach([
				'CI' => 'ZI',
				'CHEM' => 'CHE',
				'CHEM1' => 'CHE',
				'LDT' => 'LD',
				'R' => 'RUE',
				'VOIE1' => 'VOIE',
				'VLGE' => 'VGE',
			] as $source => $target)
			{
				if($typeVoie->getCode() === $target)
				{
					$this->_typesVoies[$source] = $typeVoie;
				}
			}
			// }}}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiFrInseeBanTypeVoie
	{
		$data = \mb_strtoupper(\trim((string) $data));
		
		if(isset($this->_typesVoies[$data]))
		{
			return $this->_typesVoies[$data];
		}
		
		throw new ParseException(ApiFrInseeBanTypeVoie::class, $data, 0);
	}
	
}
