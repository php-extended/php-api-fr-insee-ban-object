<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use Iterator;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\CsvStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use PhpExtended\Html\HtmlParser;
use PhpExtended\Html\HtmlParserInterface;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Throwable;

/**
 * ApiFrInseeBanBulkEndpoint class file.
 * 
 * This class represents the driver to get bulk files from the insee (which 
 * are deposed on adresse.data.gouv.fr).
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiFrInseeBanEndpoint implements ApiFrInseeBanEndpointInterface
{
	
	public const HOST = 'https://adresse.data.gouv.fr/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The html parser.
	 * 
	 * @var HtmlParserInterface
	 */
	protected HtmlParserInterface $_htmlParser;
	
	/**
	 * The temp directory path.
	 * 
	 * @var string
	 */
	protected string $_tempDirectoryPath;
	
	/**
	 * Builds a new ApiFrInseeBanEndpoint with the given http endpoint.
	 * 
	 * @param string $tempDirectoryPath
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?ReifierInterface $reifier
	 * @param ?HtmlParserInterface $htmlParser
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function __construct(
		string $tempDirectoryPath,
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?ReifierInterface $reifier = null,
		?HtmlParserInterface $htmlParser = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_reifier = $reifier ?? new Reifier();
		$this->_htmlParser = $htmlParser ?? new HtmlParser();
		
		$realpath = \realpath($tempDirectoryPath);
		if(false === $realpath)
		{
			$message = 'The given temp directory path "{dir}" does not exist on the filesystem';
			$context = ['{dir}' => $tempDirectoryPath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_dir($realpath))
		{
			$message = 'The given directory "{dir}" is not a real directory';
			$context = ['{dir}' => $realpath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_writable($realpath))
		{
			$message = 'The given directory "{dir}" is not writeable';
			$context = ['{dir}' => $realpath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		$this->_tempDirectoryPath = $realpath;
		
		$configuration = $this->_reifier->getConfiguration();
		$configuration->setParser(ApiFrInseeBanLocalisation::class, new ApiFrInseeBanLocalisationParser());
		$configuration->setParser(ApiFrInseeBanSource::class, new ApiFrInseeBanSourceParser());
		$configuration->setParser(ApiFrInseeBanTypeVoie::class, new ApiFrInseeBanTypeVoieParser());
		
		$configuration->addDateTimeFormat(ApiFrInseeBanLine::class, 'dateDerMajGroup', ['!Y-m-d H:i:s.uT']);
		$configuration->addDateTimeFormat(ApiFrInseeBanLine::class, 'dateDerMajHn', ['!Y-m-d H:i:s.uT']);
		$configuration->addDateTimeFormat(ApiFrInseeBanLine::class, 'dateDerMajPos', ['!Y-m-d H:i:s.uT']);
		
		$configuration->addFieldNameAlias(ApiFrInseeBanIgnGroup::class, 'idIgnGroup', 'ign');
		$configuration->addFieldNameAlias(ApiFrInseeBanIgnAddress::class, 'idIgnAdresse', 'ign');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getUploadDates()
	 */
	public function getUploadDates() : array
	{
		$url = self::HOST.'data/ban/adresses';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$data = $response->getBody()->__toString();
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			/** @var HtmlCollectionNodeInterface $html */
			$html = $this->_htmlParser->parse($data);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to parse html data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$nodes = $html->findAllNodesCss('section a');
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			// should not happen
			$message = 'Failed to parse css selector from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		// @codeCoverageIgnoreEnd
		
		$dates = [];
		
		/** @var HtmlCollectionNodeInterface $node */
		foreach($nodes as $node)
		{
			$href = $node->getAttribute('href');
			if(null === $href)
			{
				continue;
			}
			
			$hrefValue = \basename($href->getValue());
			
			/** @var DateTimeImmutable|false $dti */
			$dti = DateTimeImmutable::createFromFormat('!Y-m-d', $hrefValue);
			if(false !== $dti)
			{
				$dates[] = $dti;
			}
		}
		
		if(empty($dates))
		{
			throw new RuntimeException(\strtr('Failed to get dates from url {url}', ['{url}' => $url]));
		}
		
		return $dates;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getLatestDate()
	 */
	public function getLatestDate() : DateTimeInterface
	{
		$latest = DateTimeImmutable::createFromFormat('Y-m-d', '2000-01-01');
		if(false === $latest)
		{
			$message = 'Failed to parse datetime {dt}';
			$context = ['{dt}' => '2000-01-01'];
			
			throw new RuntimeException(\strtr($message, $context), -1);
		}
		
		$dates = $this->getUploadDates();
		
		foreach($dates as $date)
		{
			if($latest->getTimestamp() < $date->getTimestamp())
			{
				$latest = $date;
			}
		}
		
		return $latest;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getDepartementIds()
	 */
	public function getDepartementIds(DateTimeInterface $dti) : array
	{
		$url = self::HOST.'data/ban/export-api-gestion/'.$dti->format('Y-m-d').'/ban/';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$response = $this->_httpClient->sendRequest($request);
			$data = $response->getBody()->__toString();
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			/** @var HtmlCollectionNodeInterface $html */
			$html = $this->_htmlParser->parse($data);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to parse html data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$nodes = $html->findAllNodesCss('section a');
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			// should not happen
			$message = 'Failed to parse css selector from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		// @codeCoverageIgnoreEnd
		
		$departements = [];
		
		/** @var HtmlCollectionNodeInterface $node */
		foreach($nodes as $node)
		{
			$href = $node->getAttribute('href');
			if(null === $href)
			{
				continue;
			}
			
			$hrefValue = \basename($href->getValue());
			$matches = [];
			if(\preg_match('#ban-(\\d+|2A|2B)\\.#ui', $hrefValue, $matches))
			{
				/** @phpstan-ignore-next-line */
				$departements[] = $matches[1] ?? '';
			}
		}
		
		return $departements;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getBanLinesIterator()
	 */
	public function getBanLinesIterator(DateTimeInterface $dti, string $departementId) : Iterator
	{
		$url = self::HOST.'data/ban/export-api-gestion/'.$dti->format('Y-m-d').'/ban/ban-'.$departementId.'.csv.gz';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'ban-'.$departementId.'.csv.gz');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeBanLine::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						';',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ';', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeBanLine::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getBanIgnAddressesIterator()
	 */
	public function getBanIgnAddressesIterator(DateTimeInterface $dti, string $departementId) : Iterator
	{
		$url = self::HOST.'data/ban/export-api-gestion/'.$dti->format('Y-m-d').'/housenumber-id-ign/housenumber-id-ign-'.$departementId.'.csv.gz';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'housenumber-id-ign-'.$departementId.'.csv.gz');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeBanIgnAddress::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						';',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ';', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeBanIgnAddress::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getBanIgnGroupsIterator()
	 */
	public function getBanIgnGroupsIterator(DateTimeInterface $dti, string $departementId) : Iterator
	{
		$url = self::HOST.('data/ban/export-api-gestion/'.$dti->format('Y-m-d').'/group-id-ign/group-id-ign-'.$departementId.'.csv.gz');
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempDirectoryPath.\DIRECTORY_SEPARATOR.'group-id-ign-'.$departementId.'.csv.gz');
			$response = $this->_httpClient->sendRequest($request);
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		try
		{
			$xDownloadedFile = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($xDownloadedFile))
			{
				return $this->_reifier->reifyIterator(
					ApiFrInseeBanIgnGroup::class,
					(new CsvStringDataProvider(
						$response->getBody()->__toString(),
						true,
						';',
						'"',
						'\\',
					))->provideIterator(),
				);
			}
			
			$iterator = new CsvFileDataIterator($xDownloadedFile, true, ';', '"', '\\', 'UTF-8', 'UTF-8');
			
			return $this->_reifier->reifyIterator(ApiFrInseeBanIgnGroup::class, $iterator);
		}
		catch(ReificationThrowable|UnprovidableThrowable $exc)
		{
			$message = 'Failed to transform data from uri {uri}';
			$context = ['{uri}' => $url];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getBanLocalisationIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getBanLocalisationIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/localisation.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeBanLocalisation::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getBanSourceIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getBanSourceIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/source.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeBanSource::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeBan\ApiFrInseeBanEndpointInterface::getTypeVoieIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTypeVoieIterator() : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/type_voie.csv';
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrInseeBanTypeVoie::class, $iterator);
	}
	
}
