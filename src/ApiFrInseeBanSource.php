<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

/**
 * ApiFrInseeBanSource class file.
 * 
 * This is a simple implementation of the ApiFrInseeBanSourceInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeBanSource implements ApiFrInseeBanSourceInterface
{
	
	/**
	 * The id of the source.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The code of the source.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The definition of the source.
	 * 
	 * @var string
	 */
	protected string $_definition;
	
	/**
	 * Constructor for ApiFrInseeBanSource with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param string $definition
	 */
	public function __construct(int $id, string $code, string $definition)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setDefinition($definition);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the source.
	 * 
	 * @param int $id
	 * @return ApiFrInseeBanSourceInterface
	 */
	public function setId(int $id) : ApiFrInseeBanSourceInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the source.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of the source.
	 * 
	 * @param string $code
	 * @return ApiFrInseeBanSourceInterface
	 */
	public function setCode(string $code) : ApiFrInseeBanSourceInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the source.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the definition of the source.
	 * 
	 * @param string $definition
	 * @return ApiFrInseeBanSourceInterface
	 */
	public function setDefinition(string $definition) : ApiFrInseeBanSourceInterface
	{
		$this->_definition = $definition;
		
		return $this;
	}
	
	/**
	 * Gets the definition of the source.
	 * 
	 * @return string
	 */
	public function getDefinition() : string
	{
		return $this->_definition;
	}
	
}
