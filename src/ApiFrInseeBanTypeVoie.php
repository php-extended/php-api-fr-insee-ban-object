<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

/**
 * ApiFrInseeBanTypeVoie class file.
 * 
 * This is a simple implementation of the ApiFrInseeBanTypeVoieInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeBanTypeVoie implements ApiFrInseeBanTypeVoieInterface
{
	
	/**
	 * The id of the type voie.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The code of the type voie.
	 * 
	 * @var string
	 */
	protected string $_code;
	
	/**
	 * The full name of the type voie.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Constructor for ApiFrInseeBanTypeVoie with private members.
	 * 
	 * @param int $id
	 * @param string $code
	 * @param string $name
	 */
	public function __construct(int $id, string $code, string $name)
	{
		$this->setId($id);
		$this->setCode($code);
		$this->setName($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the type voie.
	 * 
	 * @param int $id
	 * @return ApiFrInseeBanTypeVoieInterface
	 */
	public function setId(int $id) : ApiFrInseeBanTypeVoieInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the type voie.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of the type voie.
	 * 
	 * @param string $code
	 * @return ApiFrInseeBanTypeVoieInterface
	 */
	public function setCode(string $code) : ApiFrInseeBanTypeVoieInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the type voie.
	 * 
	 * @return string
	 */
	public function getCode() : string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the full name of the type voie.
	 * 
	 * @param string $name
	 * @return ApiFrInseeBanTypeVoieInterface
	 */
	public function setName(string $name) : ApiFrInseeBanTypeVoieInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the full name of the type voie.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}
