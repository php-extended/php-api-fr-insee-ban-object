<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use DateTimeInterface;

/**
 * ApiFrInseeBanLine class file.
 * 
 * This is a simple implementation of the ApiFrInseeBanLineInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.ShortVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiFrInseeBanLine implements ApiFrInseeBanLineInterface
{
	
	/**
	 * Identifiant unique national de la position.
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « position », suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-position-566461297048461789fd46e0014c4602
	 * 
	 * @var string
	 */
	protected string $_idBanPosition;
	
	/**
	 * Identifiant unique national d'adresse.
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « housenumber »
	 * suivi d'une chaîne alphanumérique de 32 caractères, le tout séparés
	 * par « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-housenumber-129771e2247a412dadf22a178708441a
	 * 
	 * @var string
	 */
	protected string $_idBanAdresse;
	
	/**
	 * Clé nationale d'interopérabilité.
	 * Elle définit une valeur unique de l'adresse. Elle est obtenue par
	 * concaténation :
	 *  • du code INSEE de la commune sur 5 caractères ;
	 *  • de l'identifiant fantoir de la voie auquel l'adresse est rattachée
	 * sur 4 caractères ;
	 *  • du numéro de l'adresse ;
	 *  • et, le cas échéant, de l'indice de répétition, en minuscule.
	 * Tous étant séparés par « _ ».
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 90001_0019_13_bis
	 * 
	 * @var ?string
	 */
	protected ?string $_cleInterop = null;
	
	/**
	 * Identifiant national du groupe (voie, lieu-dit, ...).
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « group » suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-group-aef96b8b449f4163917f878c4d277867
	 * 
	 * @var string
	 */
	protected string $_idBanGroup;
	
	/**
	 * Identifiant FANTOIR (Fichier ANnuaire TOpographique Initialisé
	 * Réduit).
	 * Il s'agit de la concaténation du code INSEE de la commune et d'un code
	 * alphanumérique de 4 caractères.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 901050015
	 * 
	 * @var ?string
	 */
	protected ?string $_idFantoir = null;
	
	/**
	 * Il s'agit du numéro de l'adresse dans la voie, sans indice de
	 * répétition.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2 pour « 2 bis RUE PASTEUR »
	 * /!\ 0 (zéro) => Numérotation fictive dans le cas de voies ou lieux-dit
	 * sans numéros
	 * /!\ Adresse dite en « 5000 » ou « 9000 » => Ces numérotations sont
	 * attribuées par la DGFiP et correspondent à une numérotation
	 * provisoire
	 * 
	 * @var int
	 */
	protected int $_numero;
	
	/**
	 * Indice de répétition.
	 * 
	 * Mention qui complète une numérotation de voirie. L'indice de
	 * répétition permet de différencier plusieurs adresses portant le même
	 * numéro dans la même rue.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : BIS, TER, QUATER, QUINQUIES, … ou une lettre A, B, C, D, U, V, W
	 * … ou un chiffre 1, 2, 3
	 * 
	 * @var ?string
	 */
	protected ?string $_suffixe = null;
	
	/**
	 * Nom complet de la voie ou du lieu-dit de l'adresse.
	 * 
	 * Cas particulier : suite à une fusion de communes, lorsque deux voies
	 * sont homonymes sur la même commune fusionnée, le nom de l'ancienne
	 * commune est ajouté entre parenthèse à la suite du nom de la voie.
	 * Exemple : le 01/01/2016, création de la commune nouvelle de
	 * Bourgvallées en lieu et place des communes de Gourfaleur, de La
	 * Mancellière-sur-Vire, de Saint-Romphaire et de
	 * Saint-Samson-de-Bonfossé devenues déléguées.
	 * Plusieurs « rue des écoles » dans cette nouvelle commune :
	 * • RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * • RUE DES ECOLES (GOURFALEUR)
	 * • RUE DES ECOLES
	 * 
	 * Obligatoire
	 * 
	 * Ex : RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * 
	 * @var string
	 */
	protected string $_nomVoie;
	
	/**
	 * Code postal de l'adresse, servant à la distribution du courrier.
	 * 
	 * Pour plus d'information sur la correspondance CODE INSEE / CODE POSTAL
	 * consulter le site :
	 * https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/.
	 * 
	 * Non Obligatoire
	 * 
	 * @var ?string
	 */
	protected ?string $_codePostal = null;
	
	/**
	 * Nom officiel de la commune (Libellé Riche).
	 * 
	 * Obligatoire.
	 * 
	 * @var string
	 */
	protected string $_nomCommune;
	
	/**
	 * Numéro INSEE de la commune de l'adresse.
	 * 
	 * Une commune nouvelle résultant d'un regroupement de communes
	 * préexistantes se voit attribuer le code INSEE de l'ancienne commune
	 * désignée comme chef-lieu par l'arrêté préfectoral qui l'institue.
	 * En conséquence une commune change de code INSEE si un arrêté
	 * préfectoral modifie son chef-lieu.
	 * 
	 * Obligatoire
	 * 
	 * @var string
	 */
	protected string $_codeInsee;
	
	/**
	 * Complément du nom de la voie, utilisé en particulier pour conserver
	 * les communes déléguées en cas de fusions de communes.
	 * 
	 * Non Obligatoire
	 * 
	 * @var ?string
	 */
	protected ?string $_nomComplementaire = null;
	
	/**
	 * Position du nom ?
	 * 
	 * @var ?string
	 */
	protected ?string $_posName = null;
	
	/**
	 * Coordonnée cartographique de l'abscisse exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @var float
	 */
	protected float $_x;
	
	/**
	 * Coordonnée cartographique de l'ordonnée exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @var float
	 */
	protected float $_y;
	
	/**
	 * Longitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @var float
	 */
	protected float $_lon;
	
	/**
	 * Latitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @var float
	 */
	protected float $_lat;
	
	/**
	 * Type de localisation de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * @var ApiFrInseeBanLocalisationInterface
	 */
	protected ApiFrInseeBanLocalisationInterface $_typLoc;
	
	/**
	 * Source de l'adresse.
	 * 
	 * dgfip                    | Direction Générale des Finances Publiques
	 * ign                      | Institut national de l'information
	 * géographique et forestière
	 * municipal_administration | Commune
	 * 
	 * Obligatoire
	 * 
	 * @var ApiFrInseeBanSourceInterface
	 */
	protected ApiFrInseeBanSourceInterface $_source;
	
	/**
	 * Date de la dernière mise à jour du groupe.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateDerMajGroup;
	
	/**
	 * Date de la dernière mise à jour de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateDerMajHn;
	
	/**
	 * Date de la dernière mise à jour de la position.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateDerMajPos;
	
	/**
	 * Constructor for ApiFrInseeBanLine with private members.
	 * 
	 * @param string $idBanPosition
	 * @param string $idBanAdresse
	 * @param string $idBanGroup
	 * @param int $numero
	 * @param string $nomVoie
	 * @param string $nomCommune
	 * @param string $codeInsee
	 * @param float $x
	 * @param float $y
	 * @param float $lon
	 * @param float $lat
	 * @param ApiFrInseeBanLocalisationInterface $typLoc
	 * @param ApiFrInseeBanSourceInterface $source
	 * @param DateTimeInterface $dateDerMajGroup
	 * @param DateTimeInterface $dateDerMajHn
	 * @param DateTimeInterface $dateDerMajPos
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $idBanPosition, string $idBanAdresse, string $idBanGroup, int $numero, string $nomVoie, string $nomCommune, string $codeInsee, float $x, float $y, float $lon, float $lat, ApiFrInseeBanLocalisationInterface $typLoc, ApiFrInseeBanSourceInterface $source, DateTimeInterface $dateDerMajGroup, DateTimeInterface $dateDerMajHn, DateTimeInterface $dateDerMajPos)
	{
		$this->setIdBanPosition($idBanPosition);
		$this->setIdBanAdresse($idBanAdresse);
		$this->setIdBanGroup($idBanGroup);
		$this->setNumero($numero);
		$this->setNomVoie($nomVoie);
		$this->setNomCommune($nomCommune);
		$this->setCodeInsee($codeInsee);
		$this->setX($x);
		$this->setY($y);
		$this->setLon($lon);
		$this->setLat($lat);
		$this->setTypLoc($typLoc);
		$this->setSource($source);
		$this->setDateDerMajGroup($dateDerMajGroup);
		$this->setDateDerMajHn($dateDerMajHn);
		$this->setDateDerMajPos($dateDerMajPos);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets identifiant unique national de la position.
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « position », suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-position-566461297048461789fd46e0014c4602
	 * 
	 * @param string $idBanPosition
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setIdBanPosition(string $idBanPosition) : ApiFrInseeBanLineInterface
	{
		$this->_idBanPosition = $idBanPosition;
		
		return $this;
	}
	
	/**
	 * Gets identifiant unique national de la position.
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « position », suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-position-566461297048461789fd46e0014c4602
	 * 
	 * @return string
	 */
	public function getIdBanPosition() : string
	{
		return $this->_idBanPosition;
	}
	
	/**
	 * Sets identifiant unique national d'adresse.
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « housenumber »
	 * suivi d'une chaîne alphanumérique de 32 caractères, le tout séparés
	 * par « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-housenumber-129771e2247a412dadf22a178708441a
	 * 
	 * @param string $idBanAdresse
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setIdBanAdresse(string $idBanAdresse) : ApiFrInseeBanLineInterface
	{
		$this->_idBanAdresse = $idBanAdresse;
		
		return $this;
	}
	
	/**
	 * Gets identifiant unique national d'adresse.
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « housenumber »
	 * suivi d'une chaîne alphanumérique de 32 caractères, le tout séparés
	 * par « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-housenumber-129771e2247a412dadf22a178708441a
	 * 
	 * @return string
	 */
	public function getIdBanAdresse() : string
	{
		return $this->_idBanAdresse;
	}
	
	/**
	 * Sets clé nationale d'interopérabilité.
	 * Elle définit une valeur unique de l'adresse. Elle est obtenue par
	 * concaténation :
	 *  • du code INSEE de la commune sur 5 caractères ;
	 *  • de l'identifiant fantoir de la voie auquel l'adresse est rattachée
	 * sur 4 caractères ;
	 *  • du numéro de l'adresse ;
	 *  • et, le cas échéant, de l'indice de répétition, en minuscule.
	 * Tous étant séparés par « _ ».
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 90001_0019_13_bis
	 * 
	 * @param ?string $cleInterop
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setCleInterop(?string $cleInterop) : ApiFrInseeBanLineInterface
	{
		$this->_cleInterop = $cleInterop;
		
		return $this;
	}
	
	/**
	 * Gets clé nationale d'interopérabilité.
	 * Elle définit une valeur unique de l'adresse. Elle est obtenue par
	 * concaténation :
	 *  • du code INSEE de la commune sur 5 caractères ;
	 *  • de l'identifiant fantoir de la voie auquel l'adresse est rattachée
	 * sur 4 caractères ;
	 *  • du numéro de l'adresse ;
	 *  • et, le cas échéant, de l'indice de répétition, en minuscule.
	 * Tous étant séparés par « _ ».
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 90001_0019_13_bis
	 * 
	 * @return ?string
	 */
	public function getCleInterop() : ?string
	{
		return $this->_cleInterop;
	}
	
	/**
	 * Sets identifiant national du groupe (voie, lieu-dit, ...).
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « group » suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-group-aef96b8b449f4163917f878c4d277867
	 * 
	 * @param string $idBanGroup
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setIdBanGroup(string $idBanGroup) : ApiFrInseeBanLineInterface
	{
		$this->_idBanGroup = $idBanGroup;
		
		return $this;
	}
	
	/**
	 * Gets identifiant national du groupe (voie, lieu-dit, ...).
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « group » suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-group-aef96b8b449f4163917f878c4d277867
	 * 
	 * @return string
	 */
	public function getIdBanGroup() : string
	{
		return $this->_idBanGroup;
	}
	
	/**
	 * Sets identifiant FANTOIR (Fichier ANnuaire TOpographique Initialisé
	 * Réduit).
	 * Il s'agit de la concaténation du code INSEE de la commune et d'un code
	 * alphanumérique de 4 caractères.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 901050015
	 * 
	 * @param ?string $idFantoir
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setIdFantoir(?string $idFantoir) : ApiFrInseeBanLineInterface
	{
		$this->_idFantoir = $idFantoir;
		
		return $this;
	}
	
	/**
	 * Gets identifiant FANTOIR (Fichier ANnuaire TOpographique Initialisé
	 * Réduit).
	 * Il s'agit de la concaténation du code INSEE de la commune et d'un code
	 * alphanumérique de 4 caractères.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 901050015
	 * 
	 * @return ?string
	 */
	public function getIdFantoir() : ?string
	{
		return $this->_idFantoir;
	}
	
	/**
	 * Sets il s'agit du numéro de l'adresse dans la voie, sans indice de
	 * répétition.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2 pour « 2 bis RUE PASTEUR »
	 * /!\ 0 (zéro) => Numérotation fictive dans le cas de voies ou lieux-dit
	 * sans numéros
	 * /!\ Adresse dite en « 5000 » ou « 9000 » => Ces numérotations sont
	 * attribuées par la DGFiP et correspondent à une numérotation
	 * provisoire
	 * 
	 * @param int $numero
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setNumero(int $numero) : ApiFrInseeBanLineInterface
	{
		$this->_numero = $numero;
		
		return $this;
	}
	
	/**
	 * Gets il s'agit du numéro de l'adresse dans la voie, sans indice de
	 * répétition.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2 pour « 2 bis RUE PASTEUR »
	 * /!\ 0 (zéro) => Numérotation fictive dans le cas de voies ou lieux-dit
	 * sans numéros
	 * /!\ Adresse dite en « 5000 » ou « 9000 » => Ces numérotations sont
	 * attribuées par la DGFiP et correspondent à une numérotation
	 * provisoire
	 * 
	 * @return int
	 */
	public function getNumero() : int
	{
		return $this->_numero;
	}
	
	/**
	 * Sets indice de répétition.
	 * 
	 * Mention qui complète une numérotation de voirie. L'indice de
	 * répétition permet de différencier plusieurs adresses portant le même
	 * numéro dans la même rue.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : BIS, TER, QUATER, QUINQUIES, … ou une lettre A, B, C, D, U, V, W
	 * … ou un chiffre 1, 2, 3
	 * 
	 * @param ?string $suffixe
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setSuffixe(?string $suffixe) : ApiFrInseeBanLineInterface
	{
		$this->_suffixe = $suffixe;
		
		return $this;
	}
	
	/**
	 * Gets indice de répétition.
	 * 
	 * Mention qui complète une numérotation de voirie. L'indice de
	 * répétition permet de différencier plusieurs adresses portant le même
	 * numéro dans la même rue.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : BIS, TER, QUATER, QUINQUIES, … ou une lettre A, B, C, D, U, V, W
	 * … ou un chiffre 1, 2, 3
	 * 
	 * @return ?string
	 */
	public function getSuffixe() : ?string
	{
		return $this->_suffixe;
	}
	
	/**
	 * Sets nom complet de la voie ou du lieu-dit de l'adresse.
	 * 
	 * Cas particulier : suite à une fusion de communes, lorsque deux voies
	 * sont homonymes sur la même commune fusionnée, le nom de l'ancienne
	 * commune est ajouté entre parenthèse à la suite du nom de la voie.
	 * Exemple : le 01/01/2016, création de la commune nouvelle de
	 * Bourgvallées en lieu et place des communes de Gourfaleur, de La
	 * Mancellière-sur-Vire, de Saint-Romphaire et de
	 * Saint-Samson-de-Bonfossé devenues déléguées.
	 * Plusieurs « rue des écoles » dans cette nouvelle commune :
	 * • RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * • RUE DES ECOLES (GOURFALEUR)
	 * • RUE DES ECOLES
	 * 
	 * Obligatoire
	 * 
	 * Ex : RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * 
	 * @param string $nomVoie
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setNomVoie(string $nomVoie) : ApiFrInseeBanLineInterface
	{
		$this->_nomVoie = $nomVoie;
		
		return $this;
	}
	
	/**
	 * Gets nom complet de la voie ou du lieu-dit de l'adresse.
	 * 
	 * Cas particulier : suite à une fusion de communes, lorsque deux voies
	 * sont homonymes sur la même commune fusionnée, le nom de l'ancienne
	 * commune est ajouté entre parenthèse à la suite du nom de la voie.
	 * Exemple : le 01/01/2016, création de la commune nouvelle de
	 * Bourgvallées en lieu et place des communes de Gourfaleur, de La
	 * Mancellière-sur-Vire, de Saint-Romphaire et de
	 * Saint-Samson-de-Bonfossé devenues déléguées.
	 * Plusieurs « rue des écoles » dans cette nouvelle commune :
	 * • RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * • RUE DES ECOLES (GOURFALEUR)
	 * • RUE DES ECOLES
	 * 
	 * Obligatoire
	 * 
	 * Ex : RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * 
	 * @return string
	 */
	public function getNomVoie() : string
	{
		return $this->_nomVoie;
	}
	
	/**
	 * Sets code postal de l'adresse, servant à la distribution du courrier.
	 * 
	 * Pour plus d'information sur la correspondance CODE INSEE / CODE POSTAL
	 * consulter le site :
	 * https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/.
	 * 
	 * Non Obligatoire
	 * 
	 * @param ?string $codePostal
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setCodePostal(?string $codePostal) : ApiFrInseeBanLineInterface
	{
		$this->_codePostal = $codePostal;
		
		return $this;
	}
	
	/**
	 * Gets code postal de l'adresse, servant à la distribution du courrier.
	 * 
	 * Pour plus d'information sur la correspondance CODE INSEE / CODE POSTAL
	 * consulter le site :
	 * https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/.
	 * 
	 * Non Obligatoire
	 * 
	 * @return ?string
	 */
	public function getCodePostal() : ?string
	{
		return $this->_codePostal;
	}
	
	/**
	 * Sets nom officiel de la commune (Libellé Riche).
	 * 
	 * Obligatoire.
	 * 
	 * @param string $nomCommune
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setNomCommune(string $nomCommune) : ApiFrInseeBanLineInterface
	{
		$this->_nomCommune = $nomCommune;
		
		return $this;
	}
	
	/**
	 * Gets nom officiel de la commune (Libellé Riche).
	 * 
	 * Obligatoire.
	 * 
	 * @return string
	 */
	public function getNomCommune() : string
	{
		return $this->_nomCommune;
	}
	
	/**
	 * Sets numéro INSEE de la commune de l'adresse.
	 * 
	 * Une commune nouvelle résultant d'un regroupement de communes
	 * préexistantes se voit attribuer le code INSEE de l'ancienne commune
	 * désignée comme chef-lieu par l'arrêté préfectoral qui l'institue.
	 * En conséquence une commune change de code INSEE si un arrêté
	 * préfectoral modifie son chef-lieu.
	 * 
	 * Obligatoire
	 * 
	 * @param string $codeInsee
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setCodeInsee(string $codeInsee) : ApiFrInseeBanLineInterface
	{
		$this->_codeInsee = $codeInsee;
		
		return $this;
	}
	
	/**
	 * Gets numéro INSEE de la commune de l'adresse.
	 * 
	 * Une commune nouvelle résultant d'un regroupement de communes
	 * préexistantes se voit attribuer le code INSEE de l'ancienne commune
	 * désignée comme chef-lieu par l'arrêté préfectoral qui l'institue.
	 * En conséquence une commune change de code INSEE si un arrêté
	 * préfectoral modifie son chef-lieu.
	 * 
	 * Obligatoire
	 * 
	 * @return string
	 */
	public function getCodeInsee() : string
	{
		return $this->_codeInsee;
	}
	
	/**
	 * Sets complément du nom de la voie, utilisé en particulier pour
	 * conserver les communes déléguées en cas de fusions de communes.
	 * 
	 * Non Obligatoire
	 * 
	 * @param ?string $nomComplementaire
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setNomComplementaire(?string $nomComplementaire) : ApiFrInseeBanLineInterface
	{
		$this->_nomComplementaire = $nomComplementaire;
		
		return $this;
	}
	
	/**
	 * Gets complément du nom de la voie, utilisé en particulier pour
	 * conserver les communes déléguées en cas de fusions de communes.
	 * 
	 * Non Obligatoire
	 * 
	 * @return ?string
	 */
	public function getNomComplementaire() : ?string
	{
		return $this->_nomComplementaire;
	}
	
	/**
	 * Sets position du nom ?
	 * 
	 * @param ?string $posName
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setPosName(?string $posName) : ApiFrInseeBanLineInterface
	{
		$this->_posName = $posName;
		
		return $this;
	}
	
	/**
	 * Gets position du nom ?
	 * 
	 * @return ?string
	 */
	public function getPosName() : ?string
	{
		return $this->_posName;
	}
	
	/**
	 * Sets coordonnée cartographique de l'abscisse exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @param float $x
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setX(float $x) : ApiFrInseeBanLineInterface
	{
		$this->_x = $x;
		
		return $this;
	}
	
	/**
	 * Gets coordonnée cartographique de l'abscisse exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getX() : float
	{
		return $this->_x;
	}
	
	/**
	 * Sets coordonnée cartographique de l'ordonnée exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @param float $y
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setY(float $y) : ApiFrInseeBanLineInterface
	{
		$this->_y = $y;
		
		return $this;
	}
	
	/**
	 * Gets coordonnée cartographique de l'ordonnée exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getY() : float
	{
		return $this->_y;
	}
	
	/**
	 * Sets longitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @param float $lon
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setLon(float $lon) : ApiFrInseeBanLineInterface
	{
		$this->_lon = $lon;
		
		return $this;
	}
	
	/**
	 * Gets longitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getLon() : float
	{
		return $this->_lon;
	}
	
	/**
	 * Sets latitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @param float $lat
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setLat(float $lat) : ApiFrInseeBanLineInterface
	{
		$this->_lat = $lat;
		
		return $this;
	}
	
	/**
	 * Gets latitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getLat() : float
	{
		return $this->_lat;
	}
	
	/**
	 * Sets type de localisation de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * @param ApiFrInseeBanLocalisationInterface $typLoc
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setTypLoc(ApiFrInseeBanLocalisationInterface $typLoc) : ApiFrInseeBanLineInterface
	{
		$this->_typLoc = $typLoc;
		
		return $this;
	}
	
	/**
	 * Gets type de localisation de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * @return ApiFrInseeBanLocalisationInterface
	 */
	public function getTypLoc() : ApiFrInseeBanLocalisationInterface
	{
		return $this->_typLoc;
	}
	
	/**
	 * Sets source de l'adresse.
	 * 
	 * dgfip                    | Direction Générale des Finances Publiques
	 * ign                      | Institut national de l'information
	 * géographique et forestière
	 * municipal_administration | Commune
	 * 
	 * Obligatoire
	 * 
	 * @param ApiFrInseeBanSourceInterface $source
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setSource(ApiFrInseeBanSourceInterface $source) : ApiFrInseeBanLineInterface
	{
		$this->_source = $source;
		
		return $this;
	}
	
	/**
	 * Gets source de l'adresse.
	 * 
	 * dgfip                    | Direction Générale des Finances Publiques
	 * ign                      | Institut national de l'information
	 * géographique et forestière
	 * municipal_administration | Commune
	 * 
	 * Obligatoire
	 * 
	 * @return ApiFrInseeBanSourceInterface
	 */
	public function getSource() : ApiFrInseeBanSourceInterface
	{
		return $this->_source;
	}
	
	/**
	 * Sets date de la dernière mise à jour du groupe.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @param DateTimeInterface $dateDerMajGroup
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setDateDerMajGroup(DateTimeInterface $dateDerMajGroup) : ApiFrInseeBanLineInterface
	{
		$this->_dateDerMajGroup = $dateDerMajGroup;
		
		return $this;
	}
	
	/**
	 * Gets date de la dernière mise à jour du groupe.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDerMajGroup() : DateTimeInterface
	{
		return $this->_dateDerMajGroup;
	}
	
	/**
	 * Sets date de la dernière mise à jour de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @param DateTimeInterface $dateDerMajHn
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setDateDerMajHn(DateTimeInterface $dateDerMajHn) : ApiFrInseeBanLineInterface
	{
		$this->_dateDerMajHn = $dateDerMajHn;
		
		return $this;
	}
	
	/**
	 * Gets date de la dernière mise à jour de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDerMajHn() : DateTimeInterface
	{
		return $this->_dateDerMajHn;
	}
	
	/**
	 * Sets date de la dernière mise à jour de la position.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @param DateTimeInterface $dateDerMajPos
	 * @return ApiFrInseeBanLineInterface
	 */
	public function setDateDerMajPos(DateTimeInterface $dateDerMajPos) : ApiFrInseeBanLineInterface
	{
		$this->_dateDerMajPos = $dateDerMajPos;
		
		return $this;
	}
	
	/**
	 * Gets date de la dernière mise à jour de la position.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDerMajPos() : DateTimeInterface
	{
		return $this->_dateDerMajPos;
	}
	
}
