<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

/**
 * ApiFrInseeBanIgnAddress class file.
 * 
 * This is a simple implementation of the ApiFrInseeBanIgnAddressInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeBanIgnAddress implements ApiFrInseeBanIgnAddressInterface
{
	
	/**
	 * The ban address id.
	 * 
	 * @var string
	 */
	protected string $_idBanAdresse;
	
	/**
	 * The ign address id.
	 * 
	 * @var string
	 */
	protected string $_idIgnAdresse;
	
	/**
	 * Constructor for ApiFrInseeBanIgnAddress with private members.
	 * 
	 * @param string $idBanAdresse
	 * @param string $idIgnAdresse
	 */
	public function __construct(string $idBanAdresse, string $idIgnAdresse)
	{
		$this->setIdBanAdresse($idBanAdresse);
		$this->setIdIgnAdresse($idIgnAdresse);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the ban address id.
	 * 
	 * @param string $idBanAdresse
	 * @return ApiFrInseeBanIgnAddressInterface
	 */
	public function setIdBanAdresse(string $idBanAdresse) : ApiFrInseeBanIgnAddressInterface
	{
		$this->_idBanAdresse = $idBanAdresse;
		
		return $this;
	}
	
	/**
	 * Gets the ban address id.
	 * 
	 * @return string
	 */
	public function getIdBanAdresse() : string
	{
		return $this->_idBanAdresse;
	}
	
	/**
	 * Sets the ign address id.
	 * 
	 * @param string $idIgnAdresse
	 * @return ApiFrInseeBanIgnAddressInterface
	 */
	public function setIdIgnAdresse(string $idIgnAdresse) : ApiFrInseeBanIgnAddressInterface
	{
		$this->_idIgnAdresse = $idIgnAdresse;
		
		return $this;
	}
	
	/**
	 * Gets the ign address id.
	 * 
	 * @return string
	 */
	public function getIdIgnAdresse() : string
	{
		return $this->_idIgnAdresse;
	}
	
}
