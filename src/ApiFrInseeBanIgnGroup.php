<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

/**
 * ApiFrInseeBanIgnGroup class file.
 * 
 * This is a simple implementation of the ApiFrInseeBanIgnGroupInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeBanIgnGroup implements ApiFrInseeBanIgnGroupInterface
{
	
	/**
	 * The ban group id.
	 * 
	 * @var string
	 */
	protected string $_idBanGroup;
	
	/**
	 * The ign group id.
	 * 
	 * @var string
	 */
	protected string $_idIgnGroup;
	
	/**
	 * Constructor for ApiFrInseeBanIgnGroup with private members.
	 * 
	 * @param string $idBanGroup
	 * @param string $idIgnGroup
	 */
	public function __construct(string $idBanGroup, string $idIgnGroup)
	{
		$this->setIdBanGroup($idBanGroup);
		$this->setIdIgnGroup($idIgnGroup);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the ban group id.
	 * 
	 * @param string $idBanGroup
	 * @return ApiFrInseeBanIgnGroupInterface
	 */
	public function setIdBanGroup(string $idBanGroup) : ApiFrInseeBanIgnGroupInterface
	{
		$this->_idBanGroup = $idBanGroup;
		
		return $this;
	}
	
	/**
	 * Gets the ban group id.
	 * 
	 * @return string
	 */
	public function getIdBanGroup() : string
	{
		return $this->_idBanGroup;
	}
	
	/**
	 * Sets the ign group id.
	 * 
	 * @param string $idIgnGroup
	 * @return ApiFrInseeBanIgnGroupInterface
	 */
	public function setIdIgnGroup(string $idIgnGroup) : ApiFrInseeBanIgnGroupInterface
	{
		$this->_idIgnGroup = $idIgnGroup;
		
		return $this;
	}
	
	/**
	 * Gets the ign group id.
	 * 
	 * @return string
	 */
	public function getIdIgnGroup() : string
	{
		return $this->_idIgnGroup;
	}
	
}
